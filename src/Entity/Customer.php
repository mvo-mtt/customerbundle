<?php
declare(strict_types=1);

namespace Mtt\CustomerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
abstract class Customer implements CustomerInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="id_erp", type="string", length=60, nullable=true)
     */
    protected $idErp;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $lastname;

    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    protected $website;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $fax;


    public function getId():?int
    {
        return $this->id;
    }


    public function getIdErp():?string
    {
        return $this->idErp;
    }

    public function setIdErp(?string $idErp)
    {
        $this->idErp = $idErp;
    }

    public function getFirstname():?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname)
    {
        $this->firstname = $firstname;
    }


    public function getLastname():?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname)
    {
        $this->lastname = $lastname;
    }


    public function getEmail():?string
    {
        return $this->email;
    }


    public function setEmail(?string $email)
    {
        $this->email = $email;
    }


    public function getWebsite():?string
    {
        return $this->website;
    }


    public function setWebsite(?string $website)
    {
        $this->website = $website;
    }


    public function getPhone():?string
    {
        return $this->phone;
    }


    public function setPhone(?string $phone)
    {
        $this->phone = $phone;
    }


    public function getFax():?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax)
    {
        $this->fax = $fax;
    }


    public function __toString()
    {
        return $this->getFirstname().' '.$this->getLastname();
    }
}

