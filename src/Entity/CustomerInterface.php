<?php

namespace Mtt\CustomerBundle\Entity;

interface CustomerInterface
{
    public function getId():?int;

    public function getIdErp():?string;

    public function setIdErp(?string $idErp);

    public function getFirstname():?string;

    public function setFirstname(?string $firstname);

    public function getLastname():?string;

    public function setLastname(?string $lastname);

    public function getEmail():?string;

    public function setEmail(?string $email);

    public function getWebsite():?string;

    public function setWebsite(?string $website);

    public function getPhone():?string;

    public function setPhone(?string $phone);

    public function getFax():?string;

    public function setFax(?string $fax);
}